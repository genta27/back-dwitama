const {dwtExpedisi,dwtNoSj,dwtSj,prefCustomer,dwtProject,dwtProjectDetil,dwtAlat,dwtAlatDetil,dwtSjDetil,vSj} = require("../model")
const moment = require('moment')
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const db = require("../utils/database")
const { QueryTypes } = require("sequelize");
var pdf = require("pdf-creator-node");
var fs = require("fs");
var path = require('path')

module.exports = {
    getExpedisi : async(req,res) =>{
        try {
            let idExpd = req.query.expd_id;
            let dataExpedisi = await dwtExpedisi.findOne({
                where : {
                    Expd_ID : idExpd
                }
            })
            res.json(dataExpedisi)
        } catch (error) {
            console.log(error.message);
        }
    },
    addData : async(req,res) => {
        try {
            let body = req.body;
            let month = moment().month() + 1  // jan=0, dec=11
            let years = moment().year()
            let newMonth = '';
            if(month == 1){
                newMonth = 'I'
            }else if(month == 2){
                newMonth = 'II'
            }else if(month == 3){
                newMonth = 'III'
            }else if(month == 4){
                newMonth = 'IV'
            }else if(month == 5){
                newMonth = 'V'
            }else if(month == 6){
                newMonth = 'VI'
            }else if(month == 7){
                newMonth = 'VII'
            }else if(month == 8){
                newMonth = 'VIII'
            }else if(month == 9){
                newMonth = 'IX'
            }else if(month == 10){
                newMonth = 'X'
            }else if(month == 11){
                newMonth = 'XI'
            }else if(month == 12){
                newMonth = 'XII'
            }
            let custId = body.Cust_ID;
            let sess = req.session.users_login;
            let cabang = sess.cabang;
            let dataNoSj = await dwtNoSj.findOne({
                where : {
                    CABANG : cabang
                }
            });
            let noSj = dataNoSj.NOSJ;
            let newNoSj = noSj + 1;
            let nomorSj = newNoSj+'/SJ/'+custId+'-'+cabang+'/'+newMonth+'/'+years
            body.No_SJ = nomorSj;
            await dwtSj.create(body)

            let updateNoSj = {
                NOSJ : newNoSj
            };
            await dwtNoSj.update(updateNoSj,{
                where : {
                    CABANG : cabang
                }
            })
            req.flash('message','Data Surah Jalan berhasil ditambahkan');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/surat-jalan/add-surat-jalan-detail?id='+nomorSj); 
        } catch (error) {
            console.log(error.message);
        }
    },
    updateData : async(req,res) =>{
        try {
            req.flash('message','Data Surah Jalan berhasil diperbarui');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/surat-jalan/');
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataByCustomers : async(req,res) =>{
        try {
            let perPage = 10
            let currentPage = 1;
            let offset = 0;
            let customers = req.query.customers
            let dataSj = {}
            if(customers != ''){
                dataSj = await dwtSj.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                    where : {
                        'Cust_ID' : customers
                    }
                })
            }else{
                dataSj = await dwtSj.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                    
                })
            }
             
            res.json(dataSj)
        } catch (error) {
            console.log(error.message);
        }
    },
    searchData : async(req,res)=>{
        try {
            let search = req.query.search;
            let customers = req.query.customers;
            let perPage = 10
            // let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }
            if(search.length == 0){
                let dataSj = {}
                 if(customers != ''){
                    dataSj = await dwtSj.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        where : {
                            Cust_ID : customers
                        }
                    });
                 }else{
                    dataSj = await dwtSj.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                    });
                 }
                 
                res.json(dataSj);
            }else{
                let dataSj = {}
                if(customers != ''){
                    dataSj = await dwtSj.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        where : {
                            [Op.or]: [
                                {
                                    No_SJ : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Projc_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Ket : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Expd_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    TglBrk : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    TglPerkSampai : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    NoPolis : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    NamaSupir : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Cust_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                }
                            ],
                            Cust_ID : customers
                        }
                        
                    })
                }else{
                    dataSj = await dwtSj.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        where : {
                            [Op.or]: [
                                {
                                    No_SJ : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Projc_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Ket : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Expd_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    TglBrk : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    TglPerkSampai : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    NoPolis : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    NamaSupir : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Cust_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                }
                            ],
                        }
                        
                    })
                }
                
                res.json(dataSj)
            }
        } catch (error) {
            console.log(error.message);
        }
    },
    getDetailAlatByIdProject : async(req,res) => {
        try {
            let idProject = req.query.id_project;
            let dataAlat = await dwtProjectDetil.findAndCountAll({
                where : {
                    PROJC_ID : idProject,
                    
                },
                include : [
                    {
                        model : dwtAlatDetil,
                        as : '_alat_detil',
                        
                    },
                    {
                        model : dwtAlat,
                        as : '_alat'
                    }
                ]
            })
            res.json(dataAlat)
        } catch (error) {
            console.log(error.message);
        }
    },
    getAlatByIdAlatDetil : async(req,res) => {
        try {
            let idProjectDetail = req.query.id_project_detail
            let detailAlat = await dwtProjectDetil.findOne({
                where :{
                    id : idProjectDetail
                }
            })
            res.json(detailAlat)
        } catch (error) {
            console.log(error.message);
        }
    },
    addDetail : async(req,res) => {
        try {
            let body = req.body;
            let idProjectDetail = body.idProjectDetil
            let jumlahKirim = body.JmlKirim
            let dataDetailProjectDetil = await dwtProjectDetil.findByPk(idProjectDetail)
            let jumlahAwal = 0;
            if(dataDetailProjectDetil.Sisa == null){
                jumlahAwal = dataDetailProjectDetil.Jumlah
            }else{
                jumlahAwal = dataDetailProjectDetil.Sisa
            }
            let sisa = jumlahAwal - jumlahKirim
            let updateProjectDetil = {
                Sisa : sisa
            }
            body.Sisa = sisa
            await dwtSjDetil.create(body);
            await dwtProjectDetil.update(updateProjectDetil,{
                where : {
                    id : idProjectDetail
                }
            })
            res.json({
                statusCode : 200,
                data : body
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    getTableAlat : async(req,res) => {
        try {
            let noSj = req.query.no_sj
            // let data = await dwtSjDetil.findAndCountAll({
            //     where : {
            //         NoSJ : noSj
            //     },
            // })
            let query = `SELECT * FROM  dwt_sjdetil 
                            JOIN dwt_alat_detil ON dwt_sjdetil.Alat_DetIL = dwt_alat_detil.Alat_Detil
                            JOIN dwt_alat ON dwt_alat_detil.Alat_ID = dwt_alat.alat_id
                            WHERE NoSJ = '${noSj}'
                        `
            let data = await db.query(query,{
                type: QueryTypes.SELECT
            })
            res.json(data)
        } catch (error) {
            console.log(error.message);
        }
    },
    getDetilSj : async(req,res) => {
        try {
            let projcId = req.query.projc_id
            let query = `SELECT * FROM  dwt_sjdetil 
                            JOIN dwt_alat_detil ON dwt_sjdetil.Alat_DetIL = dwt_alat_detil.Alat_Detil
                            JOIN dwt_alat ON dwt_alat_detil.Alat_ID = dwt_alat.alat_id
                            WHERE PROJC_ID = '${projcId}'
                        `
            let data = await db.query(query,{
                type: QueryTypes.SELECT
            })
            res.json(data)
        } catch (error) {
            console.log(error.message);      
        }
    },
    getDataSjByCustomers : async(req,res) => {
        try {
            let custId = req.query.customers
            let data = await dwtSj.findAndCountAll({
                where : {
                    Cust_ID : custId,
                    STATUS : 0
                },
                include : [
                    {
                        model : dwtProject,
                        as : '_sj_project',
                        attributes : ['ProjectName']
                    }
                ],
            })
            res.json({
               data
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    editAlatSj : async(req,res)=>{
        try {
            let body = req.body
            let idSj = req.query.id_sj
            console.log(body.kembali);

            await dwtSjDetil.update(body,{
                where : {
                    id : idSj
                }
            })
            let data = await dwtSjDetil.findOne({
                where : {
                    id : idSj
                }
            })
            res.json({
                statusCode : 200,
                data : data
            })

        } catch (error) {
            console.log(error.message);
        }
    },
    changeStatusSj : async(req,res)=>{
        try {
            let body = req.body
            let idProjc = body.Projc_ID

            let dataUpdate = {
                STATUS : 3,
                Penerima : body.Penerima,
                JabPenerima : body.JabPenerima,
                KetTerima : body.KetTerima,
                tglTerima : body.tglTerima
            }

            await dwtSj.update(dataUpdate,{
                where : {
                    Projc_ID : idProjc
                }
            })
            req.flash('message','Data Detail Surat Terima berhasil diperbarui');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/surat-jalan/'); 

        } catch (error) {
            console.log(error.message);
        }
    },
    deleteAlat : async(req,res) => {
        try {
            let idSjDetil = req.query.id_sj_detil;
            let idProject = req.query.id_alat;
            let idAlatDetail = req.query.id_alat_detail
            let data = await dwtSjDetil.findOne({
                where : {
                    id : idSjDetil
                }
            })
            let noSj = data.NoSJ
            let jmlKirim = data.JmlKirim
            let jumlahAwal = data.Sisa
            let totalKembali = jumlahAwal + jmlKirim;
            let updateSisa = {
                Sisa : totalKembali
            }
            await dwtSjDetil.destroy({
                where : {
                    id : idSjDetil
                }
            })
            await dwtProjectDetil.update(updateSisa,{
                where : {
                    PROJC_ID : idProject,
                    Alat_DetIL : idAlatDetail
                }
            })
            res.json({
                statusCode : 200,
                data : noSj
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    confirmData : async(req,res) =>{
        try {
            req.flash('message','Data Detail Surat Jalan berhasil ditambahkan');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/surat-jalan/'); 
        } catch (error) {
            console.log(error.message);
        }
    },
    deleteData : async(req,res) => {
        try {
            let idSuratJalan = req.query.id
            await dwtSj.destroy({
                where : {
                    No_SJ : idSuratJalan
                }
            })
            req.flash('message','Data Detail Surat Jalan berhasil dihapus');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/surat-jalan/'); 
        } catch (error) {
            console.log(error.message);
        }
    },
    createPdf : async(req,res) => {
        try {
            let projcId = req.query.projc_id
            var html = fs.readFileSync(path.resolve(__dirname,'./export/template-pdf-sj.html'), "utf8");
            var options = {
                format: "A3",
                orientation: "portrait",
                border: "10mm",
              
            };
            var users = [
                {
                  name: "Shyam",
                  age: "26",
                },
                {
                  name: "Navjot",
                  age: "26",
                },
                {
                  name: "Vitthal",
                  age: "26",
                },
              ];
            let query = `SELECT * FROM v_sj WHERE Projc_ID = '${projcId}'`
            let dataSj = await db.query(query,{
                        type: QueryTypes.SELECT
                        })
            let dataSjDetil = await db.query(`SELECT * FROM dwt_sjdetil 
                                                JOIN dwt_alat_detil ON dwt_sjdetil.Alat_DetiL = dwt_alat_detil.Alat_Detil
                                                WHERE PROJC_ID = '${projcId}'`,{
                    type:QueryTypes.SELECT
            })
            var document = {
            html: html,
            data: {
                users: users,
                Projc_ID : dataSj[0].Projc_ID,
                company : dataSj[0].company,
                location : dataSj[0].Lokasi + ', '+dataSj[0].Alamat,
                no_po : dataSj[0].No_PO,
                no_sj : dataSj[0].no_sj,
                nama_supir : dataSj[0].NamaSupir,
                tgl_berangkat :dataSj[0].tglbrk,
                tgl_terima : dataSj[0].tglterima,
                base_url : `${process.env.BASE_URL}`,
                dataSjDetil : dataSjDetil

            },
            path: "./outputFiles/output.pdf",
            type: "",
            };
            pdf
            .create(document, options)
            
            .then((rest) => {
                console.log(rest);
                // res.download('./outputFiles/output.pdf')
                // res.redirect('/dashboard/surat-jalan/download')
                res.json({
                    statusCode : 200,
                    status:'success'
                })
                
            })
            .catch((error) => {
                console.error(error);
            });
            
            
        } catch (error) {
            console.log(error.message);
        }
    },
    getProjectByCustomers : async(req,res) => {
        try {
            let custId = req.query.customers
            let data = await db.query(`SELECT * FROM v_sj WHERE Cust_ID = '${custId}'`,{
                type : QueryTypes.SELECT
            })
            res.json(data)
        } catch (error) {
            console.log(error.message);
        }
    },
}