const {prefMenu,prefCustomer,dwtPo,dwtAlat,dwtAlatDetil,dwtProject,vCustPo,dwtBiayaDasar,dwtBa,dwtBaDetil, dwtProjectDetilNm,dwtSj,dwtSjDetil,dwtExpedisi, dwtProjectDetil,dwtInvoice,vSj} = require("../model");
const Sequelize = require("sequelize");
const dwt_sj = require("../model/dwt_sj");
const Op = Sequelize.Op;
const db = require("../utils/database")
const { QueryTypes } = require("sequelize");

async function getMenu(){
    const dataMenu = [];
    await prefMenu.findAll({
        where : {
            panel : 'HEAD'
        },
        order : [
            ['urut_menu','asc']
        ]
    }).then(async result => {
        for await (const dataSub of result){
            const sub   = await prefMenu.findAll({
                where : {
                    id_panel : dataSub.id_panel,
                    panel : ''
                },
                order: [
                    ['no_urut','asc']
                ]
            })
            let masterMenu = {};
            let subMenu = {};
            masterMenu.text = dataSub.text;
            masterMenu.id_menu = dataSub.id_menu;
            masterMenu.icon_panel = dataSub.icon_panel;
            masterMenu.id_panel = dataSub.id_panel;
            subMenu.text = sub.map(el => el.get({plain :true}).text)
            subMenu.id_menu = sub.map(el => el.get({plain :true}).id_menu)
            subMenu.icon_panel = sub.map(el => el.get({plain :true}).icon_panel)
            masterMenu.data = subMenu;
            dataMenu.push(masterMenu);
        }
    });
    return dataMenu;
}

module.exports = {
    index : async function(req,res) {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu(); 
            
            let data = {
                open : '',
                active : 'dashboard',
                menu : dataMenu,
                title : 'Dashboard',
                
                
            }
            res.render('dashboard/main',data);
        } catch (error) {
            console.log(error.message);
        }
        
    },
    customer : async function(req,res){
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let readAllDataCustomer = await prefCustomer.findAndCountAll();
            let totalData = readAllDataCustomer.count;
            let perPage = 10
            let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }
            let dataCustomer = await prefCustomer.findAndCountAll({
                limit : perPage,
                offset : offset
            });
            let data = {
                active : 'Customer',
                open: 'TRMS',
                menu : dataMenu,
                title : 'Dashboard',
                customer : dataCustomer.rows,
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                page : page,
                currentPage : currentPage,
                recordsTotal : totalData
            }
            res.render('dashboard/customer',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    preOrder : async (req,res) =>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let readAllDataPo = await dwtPo.findAndCountAll();
            let totalData = readAllDataPo.count;
            let perPage = 10
            let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }

            let dataPo = await dwtPo.findAndCountAll({
                limit : perPage,
                offset : offset
            });
            let dataCustomer = await prefCustomer.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - PO',
                dataPo : dataPo.rows,
                dataCustomer : dataCustomer.rows,
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                page : page,
                currentPage : currentPage,
                recordsTotal : totalData
            }
            res.render('dashboard/po',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    alat : async(req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let readAllDataAlat = await dwtAlat.findAndCountAll({
                include : [
                    {
                        model : dwtAlatDetil,
                        as : '_alat_detil',
                    }
                ]
            });
            let totalData = readAllDataAlat.count;
            let perPage = 10
            let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }

            let dataAlat = await dwtAlat.findAndCountAll({
                limit : perPage,
                offset : offset,
                include : [
                    {
                        model : dwtAlatDetil,
                        as : '_alat_detil',
                    }
                ]
            });
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Alat',
                dataAlat : dataAlat.rows,
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                page : page,
                currentPage : currentPage,
                recordsTotal : totalData
            }
            res.render('dashboard/alat',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    project : async (req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let readAllDataProject = await dwtProject.findAndCountAll({
                where : {
                    [Op.or]: [
                        {
                            Status : 1
                        },
                        {
                            Status : 0
                        }
                    ]
                }
            });
            let totalData = readAllDataProject.count;
            let perPage = 10
            let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }

            let dataProject = await dwtProject.findAndCountAll({
                limit : perPage,
                offset : offset,
                order : [['idtrans','desc']],
                where : {
                    [Op.or]: [
                        {
                            Status : 1
                        },
                        {
                            Status : 0
                        }
                    ]
                }
            });
            let dataPo = await vCustPo.findAndCountAll();
            let dataAlat = await dwtAlat.findAndCountAll();
            let dataCustomer = await prefCustomer.findAndCountAll();

            
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Project',
                dataProject : dataProject.rows,
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                page : page,
                currentPage : currentPage,
                recordsTotal : totalData,
                dataPo : dataPo.rows,
                dataAlat : dataAlat.rows,
                dataCustomer : dataCustomer.rows
            }
            res.render('dashboard/project/project',data);
        } catch (error) {
            console.log(error.message);
        }
    },  
    addProject : async(req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let dataPo = await vCustPo.findAndCountAll();
            let dataAlat = await dwtAlat.findAndCountAll();
            let dataCustomer = await prefCustomer.findAndCountAll();
            
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Project',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataPo : dataPo.rows,
                dataAlat : dataAlat.rows,
                dataCustomer : dataCustomer.rows
            }
            res.render('dashboard/project/add_project',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    addprojectDetail: async(req,res) =>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let idProject = req.query.id
            let dataMenu = await getMenu();
            let dataProject = await dwtProject.findAndCountAll({
                where : {
                    Status : 0
                },
                order : [['idtrans','desc']]
            })
            let detailProject = await dwtProject.findOne({
                where : {
                    Projc_ID : idProject
                }
            })
            // let dataAlat = await dwtAlat.findAndCountAll();
            let dataAlat = await dwtAlatDetil.findAndCountAll({
                include : [
                    {
                        model : dwtAlat,
                        as : '_alat_detil'
                    }
                ]
            })
            let dataOperator = await dwtBiayaDasar.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Project',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataProject : dataProject.rows,
                dataAlat : dataAlat.rows,
                dataOperator : dataOperator.rows,
                detailProject : detailProject
            }
            res.render('dashboard/project/add_project_detail',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    formUpdateDetailProject : async(req,res) =>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let idProject = req.query.id
            let dataMenu = await getMenu();
            let dataProject = await dwtProject.findAndCountAll({
                where : {
                    Status : 0
                },
                order : [['idtrans','desc']]
            })
            let detailProject = await dwtProject.findOne({
                where : {
                    Projc_ID : idProject
                }
            })
            // let dataAlat = await dwtAlat.findAndCountAll();
            let dataAlat = await dwtAlatDetil.findAndCountAll({
                include : [
                    {
                        model : dwtAlat,
                        as : '_alat_detil'
                    }
                ]
            })
            let dataOperator = await dwtBiayaDasar.findAndCountAll();
            let dataProjectDetil = await dwtProjectDetil.findAndCountAll({
                where : { 
                    Projc_ID : idProject
                },
                include : [
                    {
                        model : dwtAlatDetil,
                        as : '_alat_detil',
                        include : [
                            {
                                model : dwtAlat,
                                as : '_alat_detil',
                                
                            }
                        ],
                    }
                ],
            })
            let dataProjectDetilNm = await dwtProjectDetilNm.findAndCountAll({
                where : { 
                    Projc_ID : idProject
                },
                include : [
                    {
                        model : dwtBiayaDasar,
                        as : '_biaya_dasar'
                    }
                ],
            })
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Project',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataProject : dataProject.rows,
                dataAlat : dataAlat.rows,
                dataOperator : dataOperator.rows,
                detailProject : detailProject,
                dataProjectDetil : dataProjectDetil.rows,
                dataProjectDetilNm : dataProjectDetilNm.rows
            }
            res.render('dashboard/project/update-project',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    // BERITA ACARA START
    beritaAcara : async(req,res) =>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let readAllDataBa = await dwtBa.findAndCountAll();
            let totalData = readAllDataBa.count;
            let perPage = 10
            let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }

            let dataBa = await dwtBa.findAndCountAll({
                limit : perPage,
                offset : offset,
                include : [
                    {
                        model : dwtProject,
                        as : '_ba_project',
                        include : [
                            {
                                model : dwtPo,
                                as : '_ba_project_po',
                                include : [
                                    {
                                        model : prefCustomer,
                                        as : '_customer_detail',
                                        attributes : ['Company']
                                    }
                                ]
                            }
                        ]
                    }
                ]
            });
            let dataCustomer =await prefCustomer.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Berita Acara',
                dataBa : dataBa.rows,
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                page : page,
                currentPage : currentPage,
                recordsTotal : totalData,
                dataCustomer : dataCustomer.rows
            }
            res.render('dashboard/berita-acara',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    formAddBa : async(req,res) =>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let dataCustomer =await prefCustomer.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Tambah Berita Acara',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataCustomer : dataCustomer.rows
            }
            res.render('dashboard/ba/add-ba',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    formUpdateBa :async(req,res) =>{
      try {
        if(req.session.isLoggedIn != true){
            res.redirect('/');
        }
        let dataMenu = await getMenu();
        let dataCustomer =await prefCustomer.findAndCountAll();
        let dataBa = await dwtBa.findOne({
            where : {
                No_BA : req.query.no_ba
            },
            include : [
                {
                    model : dwtProject,
                    as : '_ba_project',
                    include : [
                        {
                            model : dwtPo,
                            as : '_ba_project_po',
                            include : [
                                {
                                    model : prefCustomer,
                                    as : '_customer_detail',
                                }
                            ]
                        }
                    ]
                }
            ]
        })
        let projctID = dataBa.Projc_ID
        let dataProjectDetilNm = await dwtProjectDetilNm.findAndCountAll({
            where : {
                PROJC_ID : projctID
            }
        })
        let data = {
            active : '',
            open: '',
            menu : dataMenu,
            title : 'Dashboard - Ubah Berita Acara',
            message:req.flash('message'),
            titleMessage:req.flash('title'),
            icon:req.flash('icon'),
            dataCustomer : dataCustomer.rows,
            dataBa : dataBa,
            dataProjcDetil : dataProjectDetilNm.rows
        }
        res.render('dashboard/ba/update-ba',data);
      } catch (error) {
          console.log(error.message);
      }  
    },
    // BERITA ACARA END
    // SURAT JALAN START
    suratJalan : async(req,res) =>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            // let readAllDataSj = await dwtSj.findAndCountAll();
            let totalData = 0;
            let perPage = 10
            let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }

            // let dataSuratJalan = await dwtSj.findAndCountAll({
            //     limit : perPage,
            //     offset : offset,
            //     include : [
            //         {
            //             model : dwtProject,
            //             as : '_sj_project',
                       
            //         },
            //         {
            //             model : prefCustomer,
            //             as : '_sj_customer'
            //         }
            //     ]
            // })
            // let dataSuratJalan = await vSj.findAndCountAll({
            //     limit : perPage,
            //     offset:offset
            // })
            
            let dataCustomer =await prefCustomer.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Surat Jalan',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                page : page,
                currentPage : currentPage,
                recordsTotal : totalData,
                dataCustomer : dataCustomer.rows,
                // dataSuratJalan : dataSuratJalan.rows
            }
            res.render('dashboard/surat-jalan',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    formAddSj : async(req,res)=>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let dataCustomer =await prefCustomer.findAndCountAll();
            let dataExpedisi = await dwtExpedisi.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Tambah Surat Jalan',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataCustomer : dataCustomer.rows,
                dataExpedisi : dataExpedisi.rows
            }
            res.render('dashboard/sj/add-sj',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    formAddDetailSj : async(req,res)=>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let idSj = req.query.id
            let dataMenu = await getMenu();
            let getDataSj = await dwtSj.findOne({
                where : {
                    No_SJ : idSj
                },
                include : [
                    {
                        model : dwtProject,
                        as : '_sj_project',
                    },
                    {
                        model : prefCustomer,
                        as : '_sj_customer'
                    }
                ]
            })
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Surat Jalan',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataSj : getDataSj
            }
            res.render('dashboard/sj/add-detail-sj',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    formUpdateSuratJalanDetail : async(req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let idSj = req.query.id
            let dataMenu = await getMenu();
            let getDataSj = await dwtSj.findOne({
                where : {
                    No_SJ : idSj
                },
                include : [
                    {
                        model : dwtProject,
                        as : '_sj_project',
                    },
                    {
                        model : prefCustomer,
                        as : '_sj_customer'
                    }
                ]
            })
            let query = `SELECT * FROM  dwt_sjdetil 
                            JOIN dwt_alat_detil ON dwt_sjdetil.Alat_DetIL = dwt_alat_detil.Alat_Detil
                            JOIN dwt_alat ON dwt_alat_detil.Alat_ID = dwt_alat.alat_id
                            WHERE NoSJ = '${idSj}'
                        `
            let dataAlatSj = await db.query(query,{
                type: QueryTypes.SELECT
            })
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Surat Jalan',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataSj : getDataSj,
                dataAlatSj : dataAlatSj
            }
            res.render('dashboard/sj/update-sj',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    formAddSuratTerima : async(req,res)=>{
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let dataCustomer =await prefCustomer.findAndCountAll();
            let dataExpedisi = await dwtExpedisi.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Tambah Surat Terima',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataCustomer : dataCustomer.rows,
                dataExpedisi : dataExpedisi.rows
            }
            res.render('dashboard/sj/form-surat-terima',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    formCetakSj : async(req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let dataCustomer =await prefCustomer.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Cetak Surat Jalan',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataCustomer : dataCustomer.rows,
            }
            res.render('dashboard/sj/cetak-sj',data);
        } catch (error) {
            console.log(error.message);
        }
    },
    // SURAT JALAN END
    // INVOICE
    invoice : async(req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            let readAllDataInvoice = await dwtInvoice.findAndCountAll();
            let totalData = readAllDataInvoice.count;
            let perPage = 10
            let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }

            let dataInvoice = await dwtInvoice.findAndCountAll({
                limit : perPage,
                offset : offset,
                // include : [
                //     {
                //         model : dwtProject,
                //         as : '_invoice_project',
                //         include : [
                //             {
                //                 model : dwtPo,
                //                 as : '_ba_project_po',
                //                 include : [
                //                     {
                //                         model : prefCustomer,
                //                         as : '_customer_detail',
                //                     }
                //                 ]
                //             }
                //         ]
                //     },
                // ]
            })
            
            let dataCustomer =await prefCustomer.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Invoice',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                page : page,
                currentPage : currentPage,
                recordsTotal : totalData,
                dataCustomer : dataCustomer.rows,
                dataInvoice : dataInvoice.rows
            }
            res.render('dashboard/invoice',data);
        }catch (error) {
            console.log(error.message);
        }
    },  
    formAddInvoice : async(req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            
            let dataCustomer =await prefCustomer.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Invoice',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataCustomer : dataCustomer.rows,
            }
            res.render('dashboard/invoice/add_invoice',data);
        } catch (error) {
            console.log(error.message);
        }
    },

    formAddInvoiceDetil : async(req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            
            let dataCustomer =await prefCustomer.findAndCountAll();
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Invoice Detil',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
                dataCustomer : dataCustomer.rows,
            }
            res.render('dashboard/invoice/add_invoice_detil',data);
        } catch (error) {
            console.log(error.message);
        }
    },

    exportExcelInvoice : async(req,res) => {
        try {
            if(req.session.isLoggedIn != true){
                res.redirect('/');
            }
            let dataMenu = await getMenu();
            
            let data = {
                active : '',
                open: '',
                menu : dataMenu,
                title : 'Dashboard - Invoice Detil',
                message:req.flash('message'),
                titleMessage:req.flash('title'),
                icon:req.flash('icon'),
            }
            res.render('dashboard/invoice/export-excel',data);
        } catch (error) {
            console.log(error.message);
        }
    },
 
    // INVOICE
    monitoringAsset : async function(req,res){
        try {

            let dataMenu = await getMenu();
            
            let data = {
                active : 'Monitoring Asset',
                open: 'Dashbord',
                menu : dataMenu,
                title : 'Dashboard'
            }
            res.render('dashboard/monitoring-assets',data);
        } catch (error) {
            console.log(error.message);
        }
    },
   
}