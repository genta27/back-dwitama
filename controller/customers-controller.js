const { prefCustomer } = require("../model");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const db = require("../utils/database");
const { QueryTypes} = require('sequelize');

module.exports = {
    addCustomer : async function(req,res){
        try {
            let body = req.body;
            await prefCustomer.create(body);
            req.flash('message','Data Customer berhasil ditambahkan');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/customer');
        } catch (error) {
            console.log(error.message);
        }
    },
    updateCustomer : async (req,res) =>{
        try {
            let id = req.body.Cust_id;
            let body = req.body;
            await prefCustomer.update(body,{
                where : {
                    Cust_id : id
                }
            })
            req.flash('message','Data Customer berhasil diperbarui');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/customer');
        } catch (error) {
            console.log(error.message);
        }
    },
    deleteCustomer : async function(req,res){
        try {
            let id = req.params.id;
            await prefCustomer.destroy({
                where : {
                    Cust_id : id
                }
            })
            req.flash('message','Data Customer berhasil dihapus');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/customer');
        } catch (error) {
            console.log(error.message);
        }
    },
    searchData : async(req,res) => {
        try {
            let search = req.query.search;
            let perPage = 10
            // let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }
            if(search.length == 0){
               

                let dataCustomer = await prefCustomer.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                });
                res.json(dataCustomer);
            }else{
                let dataCustomer = await prefCustomer.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                    where :{
                        [Op.or]: [
                            {
                                Cust_id : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                Company : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                alamat : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                city : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                Poscode : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                Phone : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                NPWP : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                cabang : {
                                    [Op.like] : '%'+search+'%'
                                }
                            }
                        ]
                    }
                })
                res.json(dataCustomer)
            }
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataById : async(req,res) => {
        try {
            let customers = req.query.customers;
            let data = await db.query(`SELECT * FROM dwt_po JOIN ls_customer ON dwt_po.Cust_ID = ls_customer.Cust_id WHERE ls_customer.Cust_id = '${customers}' LIMIT 1`,{
                type: QueryTypes.SELECT
            })
            res.json(data);
        } catch (error) {
            console.log(error.message);
        }
    }

    
}