const {dwtNoInv,dwtProject,dwtInvoice,dwtPo,prefCustomer,vInvoiceNew,dwtInvoiceDetil,vProjectInvoice} = require("../model")
const moment = require('moment')
const Sequelize = require("sequelize");
const { update } = require("./po-controller");
const Op = Sequelize.Op;
const db = require("../utils/database");
const { QueryTypes} = require('sequelize');
const exportDataToExcel = require("./export/invoice/exportService");

module.exports = {
    add : async(req,res) => {
        let month = moment().month() + 1  // jan=0, dec=11
        let years = moment().year()
        let newMonth = '';
        if(month == 1){
            newMonth = 'I'
        }else if(month == 2){
            newMonth = 'II'
        }else if(month == 3){
            newMonth = 'III'
        }else if(month == 4){
            newMonth = 'IV'
        }else if(month == 5){
            newMonth = 'V'
        }else if(month == 6){
            newMonth = 'VI'
        }else if(month == 7){
            newMonth = 'VII'
        }else if(month == 8){
            newMonth = 'VIII'
        }else if(month == 9){
            newMonth = 'IX'
        }else if(month == 10){
            newMonth = 'X'
        }else if(month == 11){
            newMonth = 'XI'
        }else if(month == 12){
            newMonth = 'XII'
        }
        let body = req.body;
        let custId = body.Cust_id;
        let sess = req.session.users_login;
        let cabang = sess.cabang;
        let dataNoInv = await dwtNoInv.findOne({
            where : {
                CABANG : cabang
            }
        });
        let dataProject = await dwtProject.findOne({
            where : {
                Projc_ID : body.Projc_ID
            }
        })
        let noInv = dataNoInv.NOINV;
        let newNoInv = String(parseInt(noInv.match(/(\d+)/)[0]) + 1).padStart(4, '0')
        let newYears = String(parseInt(years)).substring(2,4);
        let noInvoice = newNoInv+'.'+ dataProject.PPN_kode +'/'+'INV/'+body.Cust_ID+'-'+cabang+'/'+newMonth+'/'+newYears
        let blth = body.bulan
        let splitBlth = blth.split('-')
        let newBlth = splitBlth[0]+splitBlth[1]
        let pajak = ''
        let date = moment().format('YYYY-MM-DD')
        
        if(dataProject.PPN_kode == 'A'){
            pajak = 10
        }else{
            pajak = 0
        }
        body.Inv_id = noInvoice
        body.BLTH = newBlth
        body.PPN = pajak
        body.tglbuat = date
        body.tgl = date

        await dwtInvoice.create(body);
        let updateNoInv = {
            NOINV : newNoInv
        }
        await dwtNoInv.update(updateNoInv,{
            where : {
                CABANG : cabang
            }
        })

        req.flash('message','Data Invoice berhasil ditambahkan');
        req.flash('title','Success');
        req.flash('icon','success');
        res.redirect('/dashboard/invoice/tambah'); 
    
        // body.
    },
    addDetilInvoice : async(req,res) => {
        try {
            let body = req.body;
            let dataInvoice = await dwtInvoice.findOne({
                where :{
                    Projc_ID : body.Projc_ID
                }
            })
            let invId = dataInvoice.Inv_id
            let BLTH = dataInvoice.BLTH
            let kodeT = body.kodeT
            let dataMaterial = await db.query(`SELECT * FROM v_invoice_baru WHERE projc_id = '${body.Projc_ID}'`,{
                type : QueryTypes.SELECT
            })
            let dataInsert = []
            for(let i = 0; i < dataMaterial.length;i++){
                dataInsert.push({Blth:BLTH,Inv_id:invId,kode:dataMaterial[i].alat_detil,Nama:dataMaterial[i].nama,TYPE:dataMaterial[i].type,Satuan:dataMaterial[i].satuan,Harga:dataMaterial[i].harga,Jumlah:dataMaterial[i].jumlah,KodeT:kodeT,Lama:1,NoUrut:1,hargabln:dataMaterial[i].harga,NonPriode:1})
            }
            await dwtInvoiceDetil.bulkCreate(dataInsert)
            
        req.flash('message','Data Invoice Detil berhasil ditambahkan');
        req.flash('title','Success');
        req.flash('icon','success');
        res.redirect('/dashboard/invoice/add-detil'); 

        } catch (error) {
            console.log(error.message);
        }
    },
    searchData : async(req,res)=>{
        try {
            let search = req.query.search;
            let customers = req.query.customers;
            let perPage = 10
            // let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }
            if(search.length == 0){
                let dataSj = {}
                 if(customers != ''){
                    dataSj = await dwtInvoice.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        include : [
                            {
                                model : dwtProject,
                                as : '_invoice_project',
                                include : [
                                    {
                                        model : dwtPo,
                                        as : '_ba_project_po',
                                        include : [
                                            {
                                                model : prefCustomer,
                                                as : '_customer_detail',
                                            }
                                        ]
                                    }
                                ]
                            },
                        ],
                        where : {
                            '$_invoice_project->_ba_project_po->_customer_detail.Cust_id$' : customers
                        }
                    });
                 }else{
                    dataSj = await dwtInvoice.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        // include : [
                        //     {
                        //         model : dwtProject,
                        //         as : '_invoice_project',
                        //         include : [
                        //             {
                        //                 model : dwtPo,
                        //                 as : '_ba_project_po',
                        //                 include : [
                        //                     {
                        //                         model : prefCustomer,
                        //                         as : '_customer_detail',
                        //                     }
                        //                 ]
                        //             }
                        //         ]
                        //     },
                        // ],
                    });
                 }
                 
                res.json(dataSj);
            }else{
                let dataSj = {}
                if(customers != ''){
                    dataSj = await dwtInvoice.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        include : [
                            {
                                model : dwtProject,
                                as : '_invoice_project',
                                include : [
                                    {
                                        model : dwtPo,
                                        as : '_ba_project_po',
                                        include : [
                                            {
                                                model : prefCustomer,
                                                as : '_customer_detail',
                                            }
                                        ]
                                    }
                                ]
                            },
                        ],
                        where : {
                            [Op.or]: [
                                {
                                    Inv_id : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    nama_invoice : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Projc_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    KD_biaya : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                
                            ],
                            '$_invoice_project->_ba_project_po->_customer_detail.Cust_id$' : customers
                        }
                        
                    })
                }else{
                    dataSj = await dwtInvoice.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        // include : [
                        //     {
                        //         model : dwtProject,
                        //         as : '_invoice_project',
                        //         include : [
                        //             {
                        //                 model : dwtPo,
                        //                 as : '_ba_project_po',
                        //                 include : [
                        //                     {
                        //                         model : prefCustomer,
                        //                         as : '_customer_detail',
                        //                     }
                        //                 ]
                        //             }
                        //         ]
                        //     },
                        // ],
                        where : {
                            [Op.or]: [
                                {
                                    Inv_id : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    nama_invoice : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Projc_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    KD_biaya : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                            ],
                        }
                        
                    })
                }
                
                res.json(dataSj)
            }
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataByCustomers : async(req,res) =>{
        try {
            let perPage = 10
            let currentPage = 1;
            let offset = 0;
            let customers = req.query.customers
            let dataBa = {}
            if(customers != ''){
                dataBa = await dwtInvoice.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                    include : [
                        {
                            model : dwtProject,
                            as : '_invoice_project',
                            include : [
                                {
                                    model : dwtPo,
                                    as : '_ba_project_po',
                                    include : [
                                        {
                                            model : prefCustomer,
                                            as : '_customer_detail',
                                        }
                                    ]
                                }
                            ]
                        },
                    ],
                    where : {
                        '$_invoice_project->_ba_project_po->_customer_detail.Cust_id$' : customers
                    }
                })
            }else{
                dataBa = await dwtInvoice.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                })
            }
             
            res.json(dataBa)
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataMaterial : async(req,res) => {
        try {
            let projcId = req.query.projc_id
            let dataMaterial = await db.query(`SELECT * FROM v_invoice_baru WHERE projc_id = '${projcId}'`,{
                type : QueryTypes.SELECT
            })
            res.json({
                data : dataMaterial
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataNonMaterial : async(req,res) => {
        try {
            let projcId = req.query.projc_id
            let dataNonMaterial = await db.query(`SELECT * FROM v_biayadasar_badetil WHERE Projc_ID = '${projcId}'`,{
                type : QueryTypes.SELECT
            })
            res.json({
                data : dataNonMaterial
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataInvoiceMax : async(req,res)=>{
        try {
            let projcId = req.query.projc_id
            let dataInvoiceMax = await db.query(`SELECT * FROM v_invoice_max WHERE Projc_ID = '${projcId}'`,{
                type : QueryTypes.SELECT
            })
            res.json({
                data : dataInvoiceMax
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataInvoiceByCustomers : async(req,res) => {
        try {
            let custId = req.query.customers
            let dataProject = await db.query(`SELECT A.Projc_ID,B.ProjectName,C.Cust_ID FROM 
                                                    dwt_invoice A 
                                                        JOIN dwt_project B ON A.Projc_ID = B.Projc_ID
                                                        JOIN dwt_po C ON B.NO_PO = C.No_PO 
                                                        WHERE C.Cust_ID = '${custId}' GROUP BY Projc_ID`)
            res.json({
                data : dataProject
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    createExcel : async(req,res) => {
        try {
            let startDate = req.body.start_date;
            let endDate = req.body.end_date;
            // console.log(req.body);

            if(startDate != '' && endDate != ''){
                let newStartDate = new Date(startDate + ' 00:00:00')
                let newEndDate = new Date(endDate + ' 23:59:00')
                let dataInvoice = await vProjectInvoice.findAndCountAll({
                    where: {
                        "tgl": { [Op.between]: [newStartDate, newEndDate] }
                    },
                   
                })
                const workSheetName = 'Invoice';
                const filePath = './outputFiles/data-invoice.xlsx';
                const workSheetColumnName = [
                    "NO INVOICE",
                    "NILAI DPP",
                    "PPN",
                    "NAMA PERUSAHAAN",
                    "NAMA PROYEK",
                    "NO REFF/SPK"
                ]
                exportDataToExcel(dataInvoice.rows, workSheetColumnName, workSheetName, filePath);
                res.json({
                    status : true,
                    message : 'Excel berhasil didownload !'
                })
               
                
            }else{
               res.json({
                   status: false,
                   error : 'Mohon lengkapi data !'
               })
            }
        } catch (error) {
            console.log(error.message);
        }
    }
}