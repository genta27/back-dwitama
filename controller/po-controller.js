const { dwtPo,prefCustomer,dwtNoPo } = require("../model");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require('moment')

module.exports = {
    add : async (req,res) => {
        try {
            let body = req.body;
            let custId = body.Cust_ID
            let month = moment().month() + 1  // jan=0, dec=11
            let years = moment().year()
            let newMonth = '';
            if(month == 1){
                newMonth = 'I'
            }else if(month == 2){
                newMonth = 'II'
            }else if(month == 3){
                newMonth = 'III'
            }else if(month == 4){
                newMonth = 'IV'
            }else if(month == 5){
                newMonth = 'V'
            }else if(month == 6){
                newMonth = 'VI'
            }else if(month == 7){
                newMonth = 'VII'
            }else if(month == 8){
                newMonth = 'VIII'
            }else if(month == 9){
                newMonth = 'IX'
            }else if(month == 10){
                newMonth = 'X'
            }else if(month == 11){
                newMonth = 'XI'
            }else if(month == 12){
                newMonth = 'XII'
            }
           
            let sess = req.session.users_login;
            let cabang = sess.cabang;
            let dataNoPo = await dwtNoPo.findOne({
                where : {
                    CABANG : cabang
                }
            })
            let noPo = dataNoPo.NOPO;
            let newNoPo = parseInt(noPo) + 1;
            let idPo = newNoPo+'/'+custId+'/'+newMonth+'/'+years
            body.No_PO = idPo
            let updateNoPo = {
                NOPO : newNoPo
            }
            await dwtPo.create(body);
            await dwtNoPo.update(updateNoPo,{
                where : {
                    CABANG : cabang
                }
            })

            req.flash('message','Data PO berhasil ditambahkan');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/po');
        } catch (error) {
            console.log(error.message);
        }
    },
    update:async (req,res) =>{
        try {
            let body = req.body;
            let noPo = req.body.No_PO;
            await dwtPo.update(body,{
                where: {
                    No_PO : noPo
                }
            })
            req.flash('message','Data PO berhasil diperbarui');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/po');
        } catch (error) {
            console.log(error.message);
        }
    },
    delete : async (req,res) =>{
        try {
            let id = req.query.id
            await dwtPo.destroy({
                where :{
                    No_PO : id
                }
            })
            req.flash('message','Data PO berhasil dihapus');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/po');
        } catch (error) {
            console.log(error.message);
        }
    },
    searchData : async(req,res) => {
        try {
            let search = req.query.search;
            let customers = req.query.customers;
            
            let perPage = 10
            // let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }
            if(search.length == 0){
                let dataPo = {};
                if(customers != ''){

                    dataPo = await dwtPo.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        where : {
                            Cust_ID : customers
                        }
                    });
                }else{
                    dataPo = await dwtPo.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                    });
                }
                res.json(dataPo);
            }else{
                
                
                if(customers != ''){
                     dataPo = await dwtPo.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        where :{
                            [Op.or]: [
                                {
                                    No_PO : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Nama_PO : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Cust_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Lokasi : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Alamat : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    kolektor : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    salekode : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                            ],
                            Cust_ID : customers
                        }
                    })
                }else{
                    dataPo = await dwtPo.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        where :{
                            [Op.or]: [
                                {
                                    No_PO : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Nama_PO : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Cust_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Lokasi : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Alamat : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    kolektor : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    salekode : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                            ],
                        }
                    })
                }
               
                res.json(dataPo)
            }
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataByCustomers : async(req,res) => {
        try {
            let perPage = 10
            // let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            let customers = req.query.customers
            let dataPo = await dwtPo.findAndCountAll({
                where : {
                    Cust_ID : customers
                },
                limit : perPage,
                offset : offset,
            })
            res.json(dataPo)
        } catch (error) {
            console.log(error.message);
        }
    },
    getAllPoByCustomers : async(req,res) =>{
        try {
            let customers = req.query.customers;
            let dataPo = await dwtPo.findAndCountAll({
                where : {
                    Cust_ID: customers
                }
            })
            res.json(dataPo)
        } catch (error) {
            console.log(error.message);
        }
    }
}