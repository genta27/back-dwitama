const {prefUser} = require("../../model/");
const Swal = require("sweetalert2");
const crypto = require("crypto");
 
module.exports = {
    index: function(req,res){
        if(req.session.isLoggedIn == true){
            res.redirect('/dashboard');
        }
        let alert = {
            alert:false
        }
        res.render('login/v_login',{title:'Login',
                                    message:req.flash('message'),
                                    titleMessage:req.flash('title'),
                                    icon:req.flash('icon')});
    },

    auth: function(req,res){
        let username = req.body.username;
        let password = req.body.password;
        if(username != '' && password != ''){
            let checkUsername =  prefUser.findOne({
                where: {
                    user_id : username
                }
            }).then(function(result){
                if(result!= null){
                    let checkPassword = crypto.createHash('md5').update(password).digest('hex');
                    let dbPassword = result.passwd;
                    if(dbPassword == checkPassword){
                        req.session.users_login = result;
                        req.session.isLoggedIn = true;
                        res.redirect('/dashboard')
                    }else{
                        req.flash('message','Maaf, password yang Anda input tidak cocok!');
                        req.flash('title','Warning');
                        req.flash('icon','error');
                        res.redirect('/');
                    }
                    res.end();
                }else{
                    req.flash('message','Maaf, user id tidak ditemukan');
                    req.flash('title','Warning');
                    req.flash('icon','error');
                    res.redirect('/');
                }
            })
        }else{
            req.flash('message','Maaf, data input tidak boleh kosong!');
            req.flash('title','Warning');
            req.flash('icon','error');
            res.redirect('/');
        }
        
        
    }
}