const loginController = require("./login-controller");

module.exports = Object.freeze({
    loginController,
})