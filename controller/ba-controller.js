const {dwtNoBa,dwtBa,dwtProject,dwtPo,prefCustomer,dwtProjectDetilNm,dwtBiayaDasar,dwtBaDetil} = require("../model")
const moment = require('moment')
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

module.exports = {
    addData : async(req,res) =>{
        let month = moment().month() + 1  // jan=0, dec=11
        let years = moment().year()
        let newMonth = '';
        if(month == 1){
            newMonth = 'I'
        }else if(month == 2){
            newMonth = 'II'
        }else if(month == 3){
            newMonth = 'III'
        }else if(month == 4){
            newMonth = 'IV'
        }else if(month == 5){
            newMonth = 'V'
        }else if(month == 6){
            newMonth = 'VI'
        }else if(month == 7){
            newMonth = 'VII'
        }else if(month == 8){
            newMonth = 'VIII'
        }else if(month == 9){
            newMonth = 'IX'
        }else if(month == 10){
            newMonth = 'X'
        }else if(month == 11){
            newMonth = 'XI'
        }else if(month == 12){
            newMonth = 'XII'
        }

        let body = req.body;
        let custId = body.Cust_id;
        let sess = req.session.users_login;
        let cabang = sess.cabang;
        let dataNoBa = await dwtNoBa.findOne({
            where : {
                CABANG : cabang
            }
        });
        let noBa = dataNoBa.NOBA;
        let newNoBa = noBa + 1;
        let nomorBa = newNoBa+'/BA/'+custId+'-'+cabang+'/'+newMonth+'/'+years
        body.No_BA = nomorBa;
        await dwtBa.create(body)
        let updateNoBa = {
            NOBA : newNoBa
        };
        await dwtNoBa.update(updateNoBa,{
            where : {
                CABANG : cabang
            }
        })
        let dataOperator = await dwtProjectDetilNm.findAndCountAll({
            include : [
                {
                    model : dwtBiayaDasar,
                    as : '_biaya_dasar',
                }
            ],
            where : {
                PROJC_ID : body.Projc_ID
            }
        })
        let dataInsertBaDetil = [];
        for(let i=0; i < dataOperator.rows.length;i++){
            dataInsertBaDetil.push({No_BA:nomorBa,Projc_ID:body.Projc_ID,KD_Biaya : dataOperator.rows[i].Kd_biaya,STS : dataOperator.rows[i].sts})
        }
        await dwtBaDetil.bulkCreate(dataInsertBaDetil)

        req.flash('message','Data Berita Acara berhasil ditambahkan');
        req.flash('title','Success');
        req.flash('icon','success');
        res.redirect('/dashboard/berita-acara/'); 
    },
    deleteData : async(req,res) =>{
        try {
            let id = req.query.id
            await dwtBa.destroy({
                where: {
                    No_BA: id
                }
            })
            req.flash('message','Data Berita Acara berhasil dihapus');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/berita-acara/');
        } catch (error) {
            console.log(error.message);
        }
    },
    searchData:async(req,res) =>{
        try {
            let search = req.query.search;
            let customers = req.query.customers;

            let perPage = 10
            let currentPage = 1;
            let offset = 0;
             if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }

            let dataBa = {};
            if(search.length == 0){
                if(customers != ''){
                    dataBa = await dwtBa.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        include : [
                            {
                                model : dwtProject,
                                as : '_ba_project',
                                include : [
                                    {
                                        model : dwtPo,
                                        as : '_ba_project_po',
                                        include : [
                                            {
                                                model : prefCustomer,
                                                as : '_customer_detail',
                                                attributes : ['Company','Cust_id']
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        where : {
                            '$_ba_project->_ba_project_po->_customer_detail.Cust_id$' : customer
                        }
                    })
                }else{
                    dataBa = await dwtBa.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        include : [
                            {
                                model : dwtProject,
                                as : '_ba_project',
                                include : [
                                    {
                                        model : dwtPo,
                                        as : '_ba_project_po',
                                        include : [
                                            {
                                                model : prefCustomer,
                                                as : '_customer_detail',
                                                attributes : ['Company','Cust_id']
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                    })
                }
            }else{
                if(customers != ''){
                    dataBa = await dwtBa.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        include : [
                            {
                                model : dwtProject,
                                as : '_ba_project',
                                include : [
                                    {
                                        model : dwtPo,
                                        as : '_ba_project_po',
                                        include : [
                                            {
                                                model : prefCustomer,
                                                as : '_customer_detail',
                                                attributes : ['Company','Cust_id']
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        where : {
                            [Op.or]: [
                                {
                                    No_BA : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Projc_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Ket : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    TglBA : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    TglPerk : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    PelaksanA : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    JabPelaksana : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    '$_ba_project->_ba_project_po->_customer_detail.Cust_id$' : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                }
                            ],
                            '$_ba_project->_ba_project_po->_customer_detail.Cust_id$' : customer
                        }
                    })
                }else{
                    dataBa = await dwtBa.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        include : [
                            {
                                model : dwtProject,
                                as : '_ba_project',
                                include : [
                                    {
                                        model : dwtPo,
                                        as : '_ba_project_po',
                                        include : [
                                            {
                                                model : prefCustomer,
                                                as : '_customer_detail',
                                                attributes : ['Company','Cust_id']
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        where : {
                            [Op.or]: [
                                {
                                    No_BA : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Projc_ID : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    Ket : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    TglBA : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    TglPerk : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    PelaksanA : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    JabPelaksana : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                },
                                {
                                    '$_ba_project->_ba_project_po->_customer_detail.Cust_id$' : {
                                        [Op.like] : '%'+search+'%'
                                    }
                                }
                            ],
                        }
                    })
                }
            }
            res.json(dataBa);
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataByCustomers : async(req,res) =>{
        try {
            let perPage = 10
            let currentPage = 1;
            let offset = 0;
            let customers = req.query.customers
            let dataBa = {}
            if(customers != ''){
                dataBa = await dwtBa.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                    include : [
                        {
                            model : dwtProject,
                            as : '_ba_project',
                            include : [
                                {
                                    model : dwtPo,
                                    as : '_ba_project_po',
                                    include : [
                                        {
                                            model : prefCustomer,
                                            as : '_customer_detail',
                                            attributes : ['Company','Cust_id']
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    where : {
                        '$_ba_project->_ba_project_po->_customer_detail.Cust_id$' : customers
                    }
                })
            }else{
                dataBa = await dwtBa.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                    include : [
                        {
                            model : dwtProject,
                            as : '_ba_project',
                            include : [
                                {
                                    model : dwtPo,
                                    as : '_ba_project_po',
                                    include : [
                                        {
                                            model : prefCustomer,
                                            as : '_customer_detail',
                                            attributes : ['Company','Cust_id']
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    
                })
            }
             
            res.json(dataBa)
        } catch (error) {
            console.log(error.message);
        }
    },
    updateData:async(req,res) =>{
        try {
            let body = req.body;
            let noBa = body.No_BA;
            await dwtBa.update(body,{
                where : {
                    No_BA : noBa
                }
            })
            req.flash('message','Data Berita Acara berhasil diperbarui');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/berita-acara/'); 
        } catch (error) {
            console.log(error.message);
        }
    }
}