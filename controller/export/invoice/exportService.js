const xlsx = require("xlsx-color");
const path = require("path");

const exportExcel = (data, workSheetColumnNames, workSheetName, filePath) => {
    const workBook = xlsx.utils.book_new();
    const workSheetData = [
        workSheetColumnNames,
        ... data
    ];
    const workSheet = xlsx.utils.aoa_to_sheet(workSheetData);
    var first_sheet_name = workBook.SheetNames[0];
    var address_of_cell = 'A1';
    var arrayAddress = [
        'A','B','C','D','E','F'
    ]
    /* Get worksheet */
    var worksheet = workBook.Sheets[first_sheet_name];

    /* Find desired cell */
    for(i = 0; i < arrayAddress.length; i++){
        var desired_cell = workSheet[arrayAddress[i]+'1'];
        desired_cell.s = {
            fill : {
                patternType: "solid",
                fgColor: { rgb: "9b1f15" },
                
            },
            font : {
                color : {rgb : "ffffff"},
                bold : true
            }
            
        }
    }

    // workSheet['!cols'] = workSheetData;
    var wscols = [
        {wch:28},
        {wch:15},
        {wch:15},
        {wch:20},
        {wch:28},
        {wch:28}
    ];
    workSheet['!cols'] = wscols
    xlsx.utils.book_append_sheet(workBook, workSheet, workSheetName);
    xlsx.writeFile(workBook, path.resolve(filePath));
}


function fitToColumn(arrayOfArray) {
    // get maximum character of each column
    return arrayOfArray[0].map((a, i) => ({ wch: Math.max(...arrayOfArray.map(a2 => a2[i] ? a2[i].toString().length : 0)) }));
}

const exportDataToExcel = (dataInvoice, workSheetColumnNames, workSheetName, filePath) =>{
    const data = dataInvoice.map(invoice => {
        return [invoice.Inv_id,invoice.Nilai, invoice.NilaiPPN,invoice.Cust_ID,invoice.Inv_id,invoice.Projc_ID];
    }); 
    // console.log(data);
    exportExcel(data, workSheetColumnNames, workSheetName, filePath);
}

module.exports = exportDataToExcel;