const { prefCustomer,dwtPo,dwtNoSpk,dwtProject,dwtProjectDetil,dwtAlat,dwtAlatDetil,dwtProjectDetilNm,dwtBiayaDasar } = require("../model");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const { QueryTypes} = require('sequelize');
const db = require("../utils/database");
const moment = require('moment')

module.exports = {
    add : async(req,res) =>{
        try {
            let body = req.body;
            let custId = body.Cust_ID
            let month = moment().month() + 1  // jan=0, dec=11
            let years = moment().year()
            let newMonth = '';
            if(month == 1){
                newMonth = 'I'
            }else if(month == 2){
                newMonth = 'II'
            }else if(month == 3){
                newMonth = 'III'
            }else if(month == 4){
                newMonth = 'IV'
            }else if(month == 5){
                newMonth = 'V'
            }else if(month == 6){
                newMonth = 'VI'
            }else if(month == 7){
                newMonth = 'VII'
            }else if(month == 8){
                newMonth = 'VIII'
            }else if(month == 9){
                newMonth = 'IX'
            }else if(month == 10){
                newMonth = 'X'
            }else if(month == 11){
                newMonth = 'XI'
            }else if(month == 12){
                newMonth = 'XII'
            }
           
            let sess = req.session.users_login;
            let cabang = sess.cabang;
            let dataSpk = await dwtNoSpk.findOne({
                where : {
                    CABANG : cabang
                }
            });
            let maxIdTransProject = await dwtProject.findOne({
                order : [['idtrans','desc']]
            });
            let idTrans = maxIdTransProject.idtrans; 

            let noSpk = dataSpk.NOSPK;
            let newNoSpk = noSpk + 1;
            let splitNoPo = (req.body.NO_PO).split("-");

            let projectId = newNoSpk+'/SP/'+custId+'-'+cabang+'/'+newMonth+'/'+years;
            body.Projc_ID = projectId;
            body.idtrans = idTrans+1;
            body.Status = 0;
            await dwtProject.create(body);
            let updateSpk = {
                NOSPK : newNoSpk
            };
            await dwtNoSpk.update(updateSpk,{
                where : {
                    CABANG : cabang
                }
            })
            req.flash('message','Data Project berhasil ditambahkan');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/project/add-project-detail?id='+projectId);
        } catch (error) {
            console.log(error.message);
        }
    },
    update : async(req,res) =>{
        try {
            req.flash('message','Data Detail Project berhasil diperbarui');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/project/');
        } catch (error) {
            console.log(error.message);
        }
    },
    delete : async(req,res) =>{
        try {
            let id = req.params.id;
            await dwtProject.destroy({
                where : {
                    idtrans : id
                }
            })
            req.flash('message','Data Project berhasil dihapus');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/project');
        } catch (error) {
            console.log(error.message);
        }
    },
    getData: async(req,res) => {
        try {
            let po = [];
             let dataPo = await dwtPo.findAll().then(async result => {
                for await(const dataSub of result){
                    const sub = await prefCustomer.findAll({
                        where : {
                            Cust_id  : dataSub.Cust_ID
                        }
                    })
                    let subItem  = {};
                    subItem.no_po = dataSub.No_PO
                    subItem.company = sub.map(el => el.get({plain:true}).Company);
                    po.push(subItem);
                }
            });
            res.json(po)
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataByCustomers: async(req,res) => {
        try {
            let customers = req.query.customers
            let perPage = 10
            // let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            let dataProject = {}
            if(customers != ''){
                 dataProject = await dwtProject.findAndCountAll({
                    where : {
                        '$_ba_project_po.Cust_ID$' : customers,
                        [Op.or]: [
                            {
                                Status : 1
                            },
                            {
                                Status : 0
                            }
                        ],
                    },
                    limit : perPage,
                    offset : offset,
                    include : [
                        {
                            model : dwtPo,
                            as : '_ba_project_po',
                            
                        }
                    ]
                })
            }else{
                 dataProject = await dwtProject.findAndCountAll({
                    where : {
                        [Op.or]: [
                            {
                                Status : 1
                            },
                            {
                                Status : 0
                            }
                        ],
                    },
                    limit : perPage,
                    offset : offset,
                    include : [
                        {
                            model : dwtPo,
                            as : '_ba_project_po',
                            
                        }
                    ]
                })
            }
            
            res.json(dataProject)
        } catch (error) {
            console.log(error.message);
        }
    },
    searchData : async(req,res) =>{
        try {
            let search = req.query.search;
            let customers = req.query.customers;
            let perPage = 10
            // let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }
            if(search.length == 0){
                let dataProject = {}
                 if(customers != ''){
                    dataProject = await dwtProject.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        order : [['idtrans','desc']],
                        where :{
                            '$_ba_project_po.Cust_ID$' : customers,
                            [Op.or]: [
                                {
                                    Status : 1
                                },
                                {
                                    Status : 0
                                }
                            ],
                            
                        },
                        include : [
                            {
                                model : dwtPo,
                                as : '_ba_project_po',
                                
                            }
                        ]
                    });
                 }else{
                    dataProject = await dwtProject.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        order : [['idtrans','desc']],
                        where :{
                            [Op.or]: [
                                {
                                    Status : 1
                                },
                                {
                                    Status : 0
                                }
                            ],
                        },
                        include : [
                            {
                                model : dwtPo,
                                as : '_ba_project_po',
                                
                            }
                        ]
                    });
                 }
                 
                res.json(dataProject);
            }else{
                let dataProject = {}
                if(customers != ''){
                    dataProject = await dwtProject.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        order : [['idtrans','desc']],
                        include : [
                            {
                                model : dwtPo,
                                as : '_ba_project_po',
                                
                            }
                        ],
                        where :{
                            [Op.and]:[
                                {
                                    [Op.or] : [
                                        {
                                            Status : 1
                                        },
                                        {
                                            Status : 0
                                        }
                                    ],
                                },
                                {
                                    [Op.or]: [
                                        {
                                            Projc_ID : {
                                                [Op.like] : '%'+search+'%'
                                            }
                                        },
                                        {
                                            ProjectName : {
                                                [Op.like] : '%'+search+'%'
                                            }
                                        },
                                        {
                                            Jenis : {
                                                [Op.like] : '%'+search+'%'
                                            }
                                        },
                                        {
                                            project_title : {
                                                [Op.like] : '%'+search+'%'
                                            }
                                        },
                                        
                                    ],
                                },
                                {
                                    '$_ba_project_po.Cust_ID$' : customers 
                                }
                            ],
                             
                        },
                        
                    })
                }else{
                    dataProject = await dwtProject.findAndCountAll({
                        limit : perPage,
                        offset : offset,
                        order : [['idtrans','desc']],
                        include : [
                            {
                                model : dwtPo,
                                as : '_ba_project_po',
                                
                            }
                        ],
                        where :{
                            [Op.and]:[
                                {
                                    [Op.or] : [
                                        {
                                            Status : 1
                                        },
                                        {
                                            Status : 0
                                        }
                                    ],
                                },
                                {
                                    [Op.or]: [
                                        {
                                            Projc_ID : {
                                                [Op.like] : '%'+search+'%'
                                            }
                                        },
                                        {
                                            ProjectName : {
                                                [Op.like] : '%'+search+'%'
                                            }
                                        },
                                        {
                                            Jenis : {
                                                [Op.like] : '%'+search+'%'
                                            }
                                        },
                                        {
                                            project_title : {
                                                [Op.like] : '%'+search+'%'
                                            }
                                        },
                                        
                                    ],
                                }
                            ], 
                        },
                        
                    })
                }
                
                res.json(dataProject)
            }
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataProjectJson : async(req,res) =>{
        try {
            let idProject = req.query.id;
            let dataProject = await dwtProject.findOne({
                where : {
                    Projc_ID : idProject
                }
            });
            res.json(dataProject);
        } catch (error) {
            console.log(error.message);
        }
    },
    addProjectDetailAlat : async(req,res) => {
        try {
            let body = req.body;
            await dwtProjectDetil.create(body).then(result =>{
                res.json({
                    statusCode : 200,
                    data : body
                })
            })
           
        } catch (error) {
            console.log(error.message);
        }
    },
    updateProjectDetailAlat: async(req,res) => {
        try {
            let body = req.body;
            let id = req.body.id;
            
            await dwtProjectDetil.update(body,{
                where : {
                    id : id
                }
            }).then(result => {
                res.json({
                    statusCode : 200,
                    data : body
                })
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    deleteDataAlat: async(req,res)=>{
        try {
            let id = req.query.id;
            let oldDataProject = await dwtProjectDetil.findOne({
                where : {
                    id :id
                }
            })
            let idProject = oldDataProject.PROJC_ID;
            await dwtProjectDetil.destroy({
                where : {
                    id : id
                }
            })
            // let dataProject = await dwtProjectDetil.findOne({
            //     where : {
            //         PROJC_ID : idProject
            //     }
            // })
            res.json({
                statusCode : 200,
                data : idProject
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    getDetailProjectAlat : async(req,res) => {
        try {
            let idProject = req.query.id;
            let dataProject = await dwtProjectDetil.findAndCountAll({
                include : [
                    {
                        model : dwtAlatDetil,
                        as : '_alat_detil',
                        include : [
                            {
                                model : dwtAlat,
                                as : '_alat_detil',
                                
                            }
                        ],
                    }
                ],
               
                where : {
                    PROJC_ID : idProject
                }
            })
            res.json(dataProject);
        } catch (error) {
            console.log(error.message);
        }
    },
    addProjectDetailOperator : async(req,res) => {
        try {
            let body = req.body;
            await dwtProjectDetilNm.create(body).then(result =>{
                res.json({
                    statusCode : 200,
                    data : body
                })
            })
           
        } catch (error) {
            console.log(error.message);
        }
    },
    updateProjectDetailOperator : async(req,res) => {
        try {
            let body = req.body;
            let id = req.body.id;
            await dwtProjectDetilNm.update(body,{
                where : {
                    id : id
                }
            }).then(result =>{
                res.json({
                    statusCode : 200,
                    data: body
                })
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    deleteDataOperator : async(req,res) => {
        try {
            let id = req.query.id;
            let oldDataProject = await dwtProjectDetilNm.findOne({
                where : {
                    id :id
                }
            })
            let idProject = oldDataProject.PROJC_ID;
            await dwtProjectDetilNm.destroy({
                where : {
                    id : id
                }
            })
            // let dataProject = await dwtProjectDetilNm.findOne({
            //     where : {
            //         PROJC_ID : idProject
            //     }
            // })
            res.json({
                statusCode : 200,
                data : idProject
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    getDetailProjectOperator: async(req,res) =>{
        try {
            let projectId = req.query.id;
            let dataOperator = await dwtProjectDetilNm.findAndCountAll({
                include : [
                    {
                        model : dwtBiayaDasar,
                        as : '_biaya_dasar'
                    }
                ],
                where : {
                    PROJC_ID : projectId
                }
            })
            res.json(dataOperator);
        } catch (error) {
            console.log(error.message);
        }
    },
    getDataBiaya : async(req,res) => {
        try {
            let id = req.query.id;
            let dataBiaya  = dwtBiayaDasar.findOne({
                where : {
                    KD_biaya : id
                }
            }).then(result =>{
                res.json(result);
            })
        } catch (error) {
            console.log(error.message);
        }
    },
    addProjectDetailStatus : async(req,res) => {
        try {
            let projectId = req.body.PROJC_ID;
            let update = {
                Status : 1
            }
            await dwtProject.update(update,{
                where : {
                    Projc_ID : projectId
                }
            })
            req.flash('message','Detail Project berhasil ditambahkan');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/project');
        } catch (error) {
            console.log(error.message);
        }
    },
    
}