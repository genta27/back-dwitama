const {dwtAlat,dwtAlatDetil} = require('../model');
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

module.exports = {
    add : async(req,res)=> {
        try {
            let body = req.body;
            let alatId = '1';
            let getData = await dwtAlat.findOne({
                order : [['alat_id','desc']]
            })
            if(getData != null){
                let getId = getData.alat_id
                alatId =  parseInt(getId) + 1;
            }
            let insertAlat = {
                alat_id : alatId,
                nama : body.nama
            }

            await dwtAlat.create(insertAlat);
            delete body.nama;
            body.Alat_ID = alatId
            await dwtAlatDetil.create(body);

            req.flash('message','Data Alat berhasil ditambahkan');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/alat');

        } catch (error) {
            console.log(error.message);
        }
    },
    update: async(req,res) =>{
        try {
            let body = req.body;
            
        } catch (error) {
            console.log(error.message);
        }
    },
    delete : async(req,res) =>{
        try {
            let id = req.params.id;
            await dwtAlat.destroy({
                where : {
                    alat_id : id
                }
            })

            req.flash('message','Data Alat berhasil dihapus');
            req.flash('title','Success');
            req.flash('icon','success');
            res.redirect('/dashboard/alat');
        } catch (error) {
            console.log(error.message);
        }
    },
    getAlatJson : async(req,res) => {
        try {
            let id = req.query.id;
            let dataAlat = await dwtAlatDetil.findOne({
                where : {
                    Alat_Detil : id
                }
            })
            res.json(dataAlat);
        } catch (error) {
            console.log(error.message);
        }
    },
    searchData : async(req,res) => {
        try {
            let search = req.query.search;
            let perPage = 10
            // let page = totalData/perPage;
            let currentPage = 1;
            let offset = 0;
            if(req.query.page != null){
                currentPage = req.query.page
                offset = (currentPage * 10) - 10;
            }
            if(search.length == 0){
               

                let dataAlat = await dwtAlat.findAndCountAll({
                    limit : perPage,
                    offset : offset,
                    include : [
                        {
                            model : dwtAlatDetil,
                            as : '_alat_detil',
                        }
                    ]
                });
                res.json(dataAlat);
            }else{
                let dataAlat = await dwtAlat.findAndCountAll({
                    // attributes : ['nama','_alat_detil.type','_alat_detil.Merk','alat_id','_alat_detail.Alat_Detil','_alat_detail.ThnBuat','_alat_detail.Manufaktur'],
                    limit : perPage,
                    offset : offset,
                    include : [
                        {
                            model : dwtAlatDetil,
                            as : '_alat_detil',
                        }
                    ],
                    where :{
                        [Op.or]: [
                            {
                                nama: {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                '$_alat_detil.type$' : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                '$_alat_detil.Merk$' : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                '$_alat_detil.ThnBuat$' : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                '$_alat_detil.Manufaktur$' : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                '$_alat_detil.Suppli_ID$' : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                '$_alat_detil.HargaSewa$' : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                '$_alat_detil.HargaBeli$' : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            {
                                '$_alat_detil.Kondisi$' : {
                                    [Op.like] : '%'+search+'%'
                                }
                            },
                            
                        ]
                    }
                })
                res.json(dataAlat)
            }
        } catch (error) {
            console.log(error.message);
        }
    }
}