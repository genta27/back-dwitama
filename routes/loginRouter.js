var express  = require('express');
var router = express.Router();
const {loginController} = require('../controller/users/');

router.get('/',loginController.index);
router.post('/auth',loginController.auth);

module.exports = router;