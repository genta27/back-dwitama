let express = require("express");
let router = express.Router();
let fs = require('fs')
const dashboardController = require("../controller/dashboard-controller");
const customerController = require("../controller/customers-controller");
const poController = require("../controller/po-controller");
const alatController= require("../controller/alat-controller");
const projectController = require("../controller/project-controller");
const baController = require("../controller/ba-controller");
const sjController = require("../controller/sj-controller")
const invoiceController = require("../controller/invoice-controller")

router.get('/',dashboardController.index);
// customer
router.get('/customer',dashboardController.customer);
router.post('/customer/add',customerController.addCustomer);
router.post('/customer/update',customerController.updateCustomer);
router.get('/customer/delete/:id',customerController.deleteCustomer);
router.get('/customer/search',customerController.searchData)
router.get('/customer/get-data-by-id',customerController.getDataById)

// po
router.get('/po',dashboardController.preOrder);
router.post('/po/add',poController.add);
router.post('/po/update',poController.update);
router.get('/po/delete',poController.delete);
router.get('/po/search',poController.searchData)
router.get('/po/get-data-by-customers',poController.getDataByCustomers);
router.get('/po/get-all-po-by-customer',poController.getAllPoByCustomers)

// alat
router.get('/alat',dashboardController.alat);
router.post('/alat/add',alatController.add);
router.get('/alat/delete/:id',alatController.delete);
router.get('/alat/get-alat/',alatController.getAlatJson);
router.get('/alat/search',alatController.searchData)

// project
router.get('/project',dashboardController.project);
router.get('/project/getCombo',projectController.getData)
router.post('/project/add',projectController.add);
router.get('/project/search',projectController.searchData);
router.get('/project/delete/:id',projectController.delete)
router.get('/project/add-project',dashboardController.addProject);
router.post('/project/update',projectController.update)
router.get('/project/add-project-detail',dashboardController.addprojectDetail);
router.get('/project/get-detail',projectController.getDataProjectJson);
router.get('/project/get-detail-alat',projectController.getDetailProjectAlat);
router.post('/project/add-detail',projectController.addProjectDetailAlat);
router.post('/project/update-detail',projectController.updateProjectDetailAlat);
router.get('/project/delete-alat',projectController.deleteDataAlat);
router.get('/project/get-detail-operator',projectController.getDetailProjectOperator)
router.get('/project/get-data-biaya',projectController.getDataBiaya)
router.post('/project/add-detail-operator',projectController.addProjectDetailOperator);
router.post('/project/update-detail-operator',projectController.updateProjectDetailOperator);
router.get('/project/delete-operator',projectController.deleteDataOperator);
router.post('/project/add-project-detail-status',projectController.addProjectDetailStatus)
router.get('/project/get-project-by-customers',projectController.getDataByCustomers);
router.get('/project/update-detail-project',dashboardController.formUpdateDetailProject)


// berita acara
router.get('/berita-acara',dashboardController.beritaAcara);
router.get('/berita-acara/tambah',dashboardController.formAddBa);
router.post('/berita-acara/add',baController.addData)
router.get('/berita-acara/delete/',baController.deleteData)
router.get('/berita-acara/search',baController.searchData)
router.get('/berita-acara/get-data-by-customers',baController.getDataByCustomers)
router.get('/berita-acara/ubah/',dashboardController.formUpdateBa)
router.post('/berita-acara/update',baController.updateData);

// Surat Jalan
router.get('/surat-jalan',dashboardController.suratJalan);
router.get('/surat-jalan/tambah',dashboardController.formAddSj)
router.get('/surat-jalan/get-expedisi',sjController.getExpedisi);
router.post('/surat-jalan/add',sjController.addData);
router.get('/surat-jalan/add-surat-jalan-detail',dashboardController.formAddDetailSj)
router.get('/surat-jalan/get-data-by-customer',sjController.getDataByCustomers)
router.get('/surat-jalan/get-detail-alat',sjController.getDetailAlatByIdProject)
router.get('/surat-jalan/get-alat-by-alatid',sjController.getAlatByIdAlatDetil)
router.post('/surat-jalan/add-detail',sjController.addDetail)
router.get('/surat-jalan/get-table-alat',sjController.getTableAlat)
router.get('/surat-jalan/delete-alat',sjController.deleteAlat)
router.get('/surat-jalan/confirm',sjController.confirmData)
router.get('/surat-jalan/delete',sjController.deleteData)
router.get('/surat-jalan/update-surat-jalan-detail',dashboardController.formUpdateSuratJalanDetail)
router.get('/surat-jalan/search',sjController.searchData)
router.post('/surat-jalan/update',sjController.updateData)
router.get('/surat-jalan/form-surat-terima',dashboardController.formAddSuratTerima)
router.get('/surat-jalan/get-detil-sj',sjController.getDetilSj)
router.post('/surat-jalan/edit-alat-sj',sjController.editAlatSj)
router.post('/surat-jalan/change-status-sj',sjController.changeStatusSj)
router.get('/surat-jalan/get-data-sj-by-cust',sjController.getDataSjByCustomers)
router.get('/surat-jalan/cetak',dashboardController.formCetakSj)
router.get('/surat-jalan/get-project-by-customers',sjController.getProjectByCustomers)
router.get('/surat-jalan/createPdf',sjController.createPdf)
router.get('/surat-jalan/download',function(req,res){
    res.download('./outputFiles/output.pdf')
})
router.get('/surat-jalan/show',function(req,res){
    // console.log(__dirname + '../');
    var filePath = "./outputFiles/output.pdf";

    fs.readFile(filePath , function (err,data){
        res.contentType("application/pdf");
        res.send(data);
    });
})
// INVOICE
router.get('/invoice',dashboardController.invoice);
router.get('/invoice/tambah',dashboardController.formAddInvoice)
router.post('/invoice/add',invoiceController.add);
router.get('/invoice/search',invoiceController.searchData)
router.get('/invoice/get-data-by-customer',invoiceController.getDataByCustomers)
router.get('/invoice/add-detil',dashboardController.formAddInvoiceDetil)
router.get('/invoice/get-data-material',invoiceController.getDataMaterial)
router.get('/invoice/get-data-non-material',invoiceController.getDataNonMaterial)
router.get('/invoice/get-data-berjalan',invoiceController.getDataInvoiceMax)
router.get('/invoice/get-data-invoice-by-customers',invoiceController.getDataInvoiceByCustomers)
router.post('/invoice/add-detil',invoiceController.addDetilInvoice)
router.get('/invoice/export-excel',dashboardController.exportExcelInvoice)
router.post('/invoice/create-excel',invoiceController.createExcel)
router.get('/invoice/download-excel',function(req,res) {
    res.download('./outputFiles/data-invoice.xlsx');
})


router.get('/monitoring-assets',dashboardController.monitoringAsset);

module.exports = router;