

module.exports = (db,dataTypes) =>{
    return db.define(
        "dwt_ba",{
            No_BA : {
                primaryKey : true,
                type : dataTypes.STRING
            },
            Projc_ID : dataTypes.STRING,
            Ket : dataTypes.STRING,
            TglBA : dataTypes.DATE,
            TglPerk : dataTypes.DATE,
            PelaksanA : dataTypes.STRING,
            JabPelaksana : dataTypes.STRING,
            Ket_Pekerjaan : dataTypes.STRING,
            TYPE : dataTypes.STRING,
            tinggi : dataTypes.STRING,
            kapasitas : dataTypes.STRING,
            Pemeriksa : dataTypes.STRING,
            JabPemeriksa : dataTypes.STRING,
            KetPemerik : dataTypes.STRING,
            STS : dataTypes.INTEGER,
            TglPeriksa : dataTypes.DATE,
            INV_ID : dataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}