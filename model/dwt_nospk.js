"use strict";

module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_nospk",
        {
            CABANG : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            NOSPK : DataTypes.INTEGER,
            BLN : DataTypes.DATE
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}