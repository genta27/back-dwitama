module.exports = async(db,dataTypes) =>{
    return db.define(
        "v_sj",
        {
            no_sj : {
                primaryKey : true,
                type : dataTypes.STRING
            },
            Projc_ID : dataTypes.STRING,
            Cust_ID : dataTypes.STRING,
            Ket : dataTypes.STRING,
            tglbrk : dataTypes.DATE,
            TglPerkSampai : dataTypes.DATE,
            Expd_ID : dataTypes.STRING,
            NoPolis : dataTypes.STRING,
            NamaSupir : dataTypes.STRING,
            NoHPSupir : dataTypes.STRING,
            tglterima : dataTypes.STRING,
            Penerima : dataTypes.STRING,
            JabPenerima : dataTypes.STRING,
            ket : dataTypes.STRING,
            katagori : dataTypes.STRING,
            INV_ID : dataTypes.STRING,
            No_Po : dataTypes.STRING,
            Nama_PO : dataTypes.STRING,
            company : dataTypes.STRING,
            Lokasi : dataTypes.STRING,
            Alamat : dataTypes.STRING,
            salekode : dataTypes.STRING,
            kolektor : dataTypes.STRING,
            ProjectName : dataTypes.STRING,
            PPN_kode : dataTypes.INTEGER,
            Status : dataTypes.STRING,
            TGLMULAI : dataTypes.STRING,
            jenis : dataTypes.STRING,
            project_title : dataTypes.STRING,
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}