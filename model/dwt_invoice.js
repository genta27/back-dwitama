const datetime = require('node-datetime');

module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_invoice",
        {
            BLTH :  DataTypes.INTEGER,
            nama_invoice : DataTypes.STRING,
            Inv_id : {
                primaryKey : true,
                type : DataTypes.STRING
            },
            Projc_ID : DataTypes.STRING,
            KD_biaya : DataTypes.STRING,
            tglbuat : {
                type :DataTypes.DATE,
                get() {
                    return datetime.create(this.getDataValue('tglbuat')).format('Y-m-d')
                }
            },
            PPN: DataTypes.INTEGER,
            Discont : DataTypes.INTEGER,
            Nilai : DataTypes.INTEGER,
            NilaiPPN : DataTypes.INTEGER,
            NilaiDisc : DataTypes.INTEGER,
            Total : DataTypes.INTEGER,
            tgl : DataTypes.DATE,
            ke : DataTypes.INTEGER,
            STATUS : DataTypes.INTEGER,
            KODE_PPN : DataTypes.STRING,
            TGLCETAK : DataTypes.DATE,
            KODERESI : DataTypes.STRING,
            TGLTERIMA : DataTypes.DATE,
            No_Inv : DataTypes.INTEGER,
            kode_ext : DataTypes.INTEGER,
            nonPriode : DataTypes.INTEGER,
            stspriode : DataTypes.INTEGER,
            kode_inv : DataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}