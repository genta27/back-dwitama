"use strict";

module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_nosj",
        {
            CABANG : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            NOSJ : DataTypes.INTEGER,
            BLN : DataTypes.DATE
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}