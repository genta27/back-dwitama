module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_expedisi",
        {
            Expd_ID : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            Nm_Supir : DataTypes.STRING,
            NoTruk : DataTypes.STRING,
            NoTelp: DataTypes.STRING,
            katagori : DataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}