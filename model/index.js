const Sequelize = require("sequelize");
const db = require("../utils/database");

const PrefUser = require("./prf_user");
const PrefMenu = require("./prf_menu");
const PrefCustomer = require("./prf_customer");
const DwtPo =require("./dwt_po");
const DwtAlat = require("./dwt_alat");
const DwtAlatDetil = require("./dwt_alat_detil");
const DwtProject = require("./dwt_project");
const DwtNoSpk =require("./dwt_nospk");
const DwtProjectDetil = require("./dwt_project_detil");
const DwtBiayaDasar = require("./dwt_biayadasar");
const DwtProjectDetilNm = require("./dwt_project_detilnm");
const DwtBa = require("./dwt_ba");
const DwtBaDetil = require("./dwt_ba_detil");
const DwtNoBa = require("./dwt_no_ba")
const DwtSj = require("./dwt_sj");
const DwtSjDetil = require("./dwt_sj_detil");
const DwtExpedisi = require("./dwt_expedisi");
const DwtNoSj = require("./dwt_nosj")
const DwtNoPo = require("./dwt_nopo")
const DwtNoInv = require("./dwt_noinv")
const DwtInvoice = require("./dwt_invoice")
const DwtInvoiceDetil = require("./dwt_invoice_detil")

// VIEW
const VCustPO = require("./v_cust_po");
const dwt_po = require("./dwt_po");
const VSj = require("./v_sj")
const VInvoiceBaru = require("./v_invoice_baru")
const VProjectInvoice = require("./view_project_invoice");

const prefUser = PrefUser(db,Sequelize);
const prefMenu = PrefMenu(db,Sequelize);
const prefCustomer = PrefCustomer(db,Sequelize);
const dwtPo = DwtPo(db,Sequelize);
const dwtAlat = DwtAlat(db,Sequelize);
const dwtAlatDetil = DwtAlatDetil(db,Sequelize);
const dwtProject = DwtProject(db,Sequelize);
const vCustPo = VCustPO(db,Sequelize);
const dwtNoSpk = DwtNoSpk(db,Sequelize);
const dwtProjectDetil = DwtProjectDetil(db,Sequelize);
const dwtBiayaDasar = DwtBiayaDasar(db,Sequelize);
const dwtProjectDetilNm = DwtProjectDetilNm(db,Sequelize);
const dwtBa = DwtBa(db,Sequelize);
const dwtBaDetil = DwtBaDetil(db,Sequelize);
const dwtNoBa = DwtNoBa(db,Sequelize)
const dwtSj = DwtSj(db,Sequelize);
const dwtSjDetil = DwtSjDetil(db,Sequelize)
const dwtExpedisi = DwtExpedisi(db,Sequelize)
const dwtNoSj = DwtNoSj(db,Sequelize)
const dwtNoPo = DwtNoPo(db,Sequelize)
const dwtInvoice = DwtInvoice(db,Sequelize)
const dwtNoInv = DwtNoInv(db,Sequelize)
const vSj = VSj(db,Sequelize)
const vInvoiceNew = VInvoiceBaru(db,Sequelize)
const dwtInvoiceDetil = DwtInvoiceDetil(db,Sequelize)
const vProjectInvoice = VProjectInvoice(db,Sequelize)

dwtAlat.belongsTo(dwtAlatDetil,{
  foreignKey : "alat_id",
  as : "_alat_detil",
  targetKey: "Alat_ID",
})

dwtAlatDetil.belongsTo(dwtAlat,{
  foreignKey : 'Alat_ID',
  as : '_alat_detil',
  targetKey : 'alat_id'
})

dwtPo.belongsTo(prefCustomer,{
  foreignKey : 'Cust_ID',
  as : '_customer_detail',
  targetKey : 'Cust_id',
})

dwtProjectDetil.belongsTo(dwtAlat,{
  foreignKey : 'Alat_ID',
  as : '_alat',
  targetKey: 'alat_id'
})

dwtProjectDetil.belongsTo(dwtAlatDetil,{
  foreignKey : 'Alat_DetIL',
  as : '_alat_detil',
  targetKey : 'Alat_Detil'
})

dwtProjectDetilNm.belongsTo(dwtBiayaDasar,{
  foreignKey : 'Kd_biaya',
  as : '_biaya_dasar',
  targetKey: 'KD_biaya'
})

dwtBa.belongsTo(dwtProject,{
  foreignKey:'Projc_ID',
  as : '_ba_project',
  targetKey : 'Projc_ID'
})

dwtProject.belongsTo(dwtPo,{
  foreignKey: 'NO_PO',
  as : '_ba_project_po',
  targetKey : 'No_PO'
})

dwtSj.belongsTo(dwtProject,{
  foreignKey:'Projc_ID',
  as : '_sj_project',
  targetKey : 'Projc_ID'
})

dwtSj.belongsTo(prefCustomer,{
  foreignKey : 'Cust_ID',
  as : '_sj_customer',
  targetKey:'Cust_id'
})

dwtSjDetil.belongsTo(dwtAlat,{
  foreignKey : 'Alat_ID',
  as : '_sj_alat',
  targetKey : 'alat_id'
})

dwtSjDetil.belongsTo(dwtAlatDetil,{
  foreignKey : 'Alat_DetIL',
  as : '_sj_alat_detail',
  targetKey : 'Alat_Detil'
})

dwtSjDetil.belongsTo(dwtProjectDetil,{
  foreignKey : 'PROJC_ID',
  as : '_sj_project_detil',
  targetKey : 'PROJC_ID'
})

dwtInvoice.belongsTo(dwtProject,{
  foreignKey : 'Projc_ID',
  as : '_invoice_project',
  targetKey: 'Projc_ID'
})

/* Test Connection */
async function authenticate() {
    try {
      await db.authenticate();
      console.log("Connection has been established successfully.");
    } catch (error) {
      console.error("Unable to connect to the database:", error);
    }
  }
  authenticate();
  

module.exports = Object.freeze({
    prefUser,
    prefMenu,
    prefCustomer,
    dwtPo,
    dwtAlat,
    dwtAlatDetil,
    dwtProject,
    vCustPo,
    dwtNoSpk,
    dwtProjectDetil,
    dwtProjectDetilNm,
    dwtBiayaDasar,
    dwtBa,
    dwtBaDetil,
    dwtNoBa,
    dwtSj,
    dwtSjDetil,
    dwtExpedisi,
    dwtNoSj,
    dwtNoPo,
    dwtInvoice,
    dwtNoInv,
    vSj,
    vInvoiceNew,
    dwtInvoiceDetil,
    vProjectInvoice
});