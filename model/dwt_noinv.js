"use strict";

module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_noinv",
        {
            CABANG : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            NOINV : DataTypes.STRING,
            BLN : DataTypes.DATE
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}