module.exports = (db,DataTypes) => {
    return db.define(
        "dwt_alat",
        {
            alat_id : {
                type : DataTypes.STRING,
                primaryKey: true
            },
            nama : DataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}