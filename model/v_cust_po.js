"use strict";

module.exports = (db,DataTypes) => {
    return db.define(
        "v_cust_po",
        {
            cust_id : DataTypes.STRING,
            Company : DataTypes.STRING,
            alamat : DataTypes.STRING,
            NPWP : DataTypes.STRING,
            Phone : DataTypes.STRING,
            cabang : DataTypes.STRING,
            No_PO : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            Nama_PO : DataTypes.STRING,
            Status : DataTypes.STRING,
            Lokasi : DataTypes.STRING,
            kolektor : DataTypes.STRING,
            salekode : DataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}