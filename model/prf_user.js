module.exports = (db,DataTypes) => {
    return db.define(
        "prf_user",
        {
            user_id : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            nama : DataTypes.STRING,
            passwd : DataTypes.STRING,
            kode_group : DataTypes.STRING,
            email : DataTypes.STRING,
            telp : DataTypes.STRING,
            aktif : DataTypes.INTEGER,
            tgl : {
                type: DataTypes.DATE,
                defaultValue: DataTypes.fn('NOW'),
            },
            nmphoto : DataTypes.STRING,
            pathphoto : DataTypes.STRING,
            nip : DataTypes.STRING,
            cabang : DataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
            
        }
    )
}