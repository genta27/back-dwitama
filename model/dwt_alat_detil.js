module.exports = (db,DataTypes) => {
    return db.define(
        "dwt_alat_detil",
        {
            Alat_Detil : {
                primaryKey : true,
                type : DataTypes.STRING,
                autoIncrement : true
            },
            Alat_ID : DataTypes.STRING,
            type : DataTypes.STRING,
            Merk : DataTypes.STRING,
            ThnBuat : DataTypes.DATE,
            Manufaktur : DataTypes.STRING,
            Suppli_ID : DataTypes.STRING,
            HargaSewa : DataTypes.INTEGER,
            HargaBeli : DataTypes.INTEGER,
            Kondisi: DataTypes.STRING,
            Status : DataTypes.INTEGER
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}