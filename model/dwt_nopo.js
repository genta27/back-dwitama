"use strict";

module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_nopo",
        {
            CABANG : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            NOPO : DataTypes.INTEGER,
            BLN : DataTypes.DATE
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}