

module.exports = (db,dataTypes) =>{
    return db.define(
        "dwt_sj",{
            No_SJ : {
                primaryKey : true,
                type : dataTypes.STRING
            },
            Projc_ID : dataTypes.STRING,
            Cust_ID : dataTypes.STRING,
            Ket : dataTypes.STRING,
            TglBrk : dataTypes.DATE,
            TglPerkSampai : dataTypes.DATE,
            Expd_ID : dataTypes.STRING,
            NoPolis : dataTypes.STRING,
            NamaSupir : dataTypes.STRING,
            NoHPSupir : dataTypes.STRING,
            STATUS : dataTypes.INTEGER,
            tglTerima : dataTypes.STRING,
            Penerima : dataTypes.STRING,
            JabPenerima : dataTypes.STRING,
            KetTerima : dataTypes.STRING,
            katagori : dataTypes.STRING,
            INV_ID : dataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}