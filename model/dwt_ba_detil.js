

module.exports = (db,dataTypes) =>{
    return db.define(
        "dwt_badetil",{
            id : {
                primaryKey : true,
                type : dataTypes.STRING,
                autoIncrement : true
            },
            No_BA : dataTypes.STRING,
            Projc_ID : dataTypes.STRING,
            KD_Biaya : dataTypes.STRING,
            STS : dataTypes.INTEGER,
            INV_ID : dataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}