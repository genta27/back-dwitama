"use strict";

module.exports = (db,DataTypes) => {
    return db.define(
        "dwt_po",
        {
            No_PO : {
                type : DataTypes.STRING,
                primaryKey: true
            },
            Nama_PO : DataTypes.STRING,
            Cust_ID : DataTypes.STRING,
            Status : DataTypes.STRING,
            tglPO : DataTypes.DATE,
            Keterangan : DataTypes.STRING,
            Lokasi : DataTypes.STRING,
            Alamat : DataTypes.STRING,
            tgl : DataTypes.DATE,
            kolektor : DataTypes.STRING,
            salekode : DataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}