"use strict";

module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_projectdetilnm",
        {
            id : {
                type : DataTypes.INTEGER,
                autoIncrement :true,
                primaryKey : true
            },
            PROJC_ID : DataTypes.STRING,
            NO_PO : DataTypes.STRING,
            Kd_biaya : DataTypes.STRING,
            Satuan : DataTypes.STRING,
            Harga : DataTypes.INTEGER,
            Jumlah : DataTypes.INTEGER,
            sts : DataTypes.INTEGER,
            kode : DataTypes.STRING,
            tgl : DataTypes.DATE
        },{
            freezeTableName : true,
            timestamps:false
        }
    )
}