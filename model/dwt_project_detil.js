"use strict";
module.exports = (db,DataTypes)=>{
    return db.define(
        "dwt_projectdetil",
        {
            id : {
                type : DataTypes.INTEGER,
                primaryKey : true,
                autoIncrement :true
            },
            PROJC_ID : DataTypes.STRING,
            NO_PO : DataTypes.STRING,
            Alat_ID : DataTypes.STRING,
            Alat_DetIL : DataTypes.STRING,
            Satuan : DataTypes.STRING,
            Harga : DataTypes.INTEGER,
            Jumlah : DataTypes.INTEGER,
            Sisa : DataTypes.INTEGER,
            JumlahAwal : DataTypes.INTEGER
        },
        {
            freezeTableName : true,
            timestamps: false
        }
    )
}