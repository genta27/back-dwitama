module.exports = async(db,DataTypes) =>{
    return db.define(
        "v_invoice_grup",
        {
            JENIS : DataTypes.STRING,
            no_SJ :{
                type : DataTypes.STRING,
                primaryKey : true
            },
            projc_id : DataTypes.STRING,
            tglbrk : DataTypes.DATE,
            tglterima : DataTypes.DATE,
            alat_id : DataTypes.STRING,
            alat_detil : DataTypes.STRING,
            nama : DataTypes.STRING,
            type : DataTypes.STRING,
            merk : DataTypes.STRING,
            satuan : DataTypes.STRING,
            harga : DataTypes.INTEGER,
            jumlah : DataTypes.INTEGER,
            jmlkirim : DataTypes.INTEGER,
            inv_id : DataTypes.STRING,
            status : DataTypes.INTEGER,
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}