"use strict";

module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_noba",
        {
            CABANG : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            NOBA : DataTypes.INTEGER,
            BLN : DataTypes.DATE
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}