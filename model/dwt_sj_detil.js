

module.exports = (db,dataTypes) =>{
    return db.define(
        "dwt_sjdetil",{
            id : {
                primaryKey : true,
                type : dataTypes.STRING,
                autoIncrement : true
            },
            PROJC_ID : dataTypes.STRING,
            NoSJ : dataTypes.STRING,
            Alat_ID : dataTypes.DATE,
            Alat_DetIL : dataTypes.DATE,
            Satuan : dataTypes.STRING,
            Harga : dataTypes.STRING,
            Jumlah : dataTypes.STRING,
            JmlKirim : dataTypes.STRING,
            Sisa : dataTypes.STRING,
            Warna : dataTypes.STRING,
            Jenis : dataTypes.STRING,
            Kembali : dataTypes.STRING,
            Kurang : dataTypes.STRING,
            INV_ID : dataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}