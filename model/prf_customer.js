"use strict";
module.exports = (db,DataTypes) =>{
    return db.define(
        "ls_customer",
        {
            Cust_id : {
                type : DataTypes.STRING,
                primaryKey : true
            },
            Company: DataTypes.STRING,
            alamat : DataTypes.STRING,
            city : DataTypes.STRING,
            Poscode : DataTypes.STRING,
            Phone : DataTypes.STRING,
            NPWP : DataTypes.STRING,
            cabang : DataTypes.STRING,
            status : DataTypes.INTEGER,
            jns_customer : DataTypes.INTEGER
        },
        {
            freezeTableName : true,
            timestamps:false
        }
    )
}