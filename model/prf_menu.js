module.exports = (db,DataTypes)=>{
    return db.define(
        "prf_menu",
        {
            id_menu : {
                primaryKey: true,
                type : DataTypes.STRING
            },
            text : DataTypes.STRING,
            mapping : DataTypes.STRING,
            url : DataTypes.STRING,
            icon : DataTypes.STRING,
            panel : DataTypes.STRING,
            id_panel : DataTypes.STRING,
            no_urut : DataTypes.INTEGER,
            icon_panel : DataTypes.STRING,
            app : DataTypes.STRING,
            urut_menu : DataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps:false
        }
    )
}