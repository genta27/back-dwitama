module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_invdetil",
        {
            id : {
                primaryKey: true,
                type : DataTypes.INTEGER
            },
            Blth :  DataTypes.INTEGER,
            Inv_id : DataTypes.STRING,
            kode : DataTypes.STRING,
            Nama : DataTypes.STRING,
            TYPE : DataTypes.STRING,
            Satuan : DataTypes.STRING,
            Harga : DataTypes.NUMBER,
            Jumlah : DataTypes.INTEGER,
            KodeT : DataTypes.STRING,
            Priode1 : DataTypes.DATE,
            Priode2 : DataTypes.DATE,
            Lama :DataTypes.INTEGER,
            Priode : DataTypes.DATE,
            NoUrut : DataTypes.INTEGER,
            hargabln : DataTypes.NUMBER,
            NonPriode : DataTypes.INTEGER
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}