module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_project",
        {
            idtrans : {
                type : DataTypes.INTEGER,
                primaryKey : true
            },
            Projc_ID : DataTypes.STRING,
            ProjectName : DataTypes.STRING,
            NO_PO : DataTypes.STRING,
            PPN_kode : DataTypes.STRING,
            Status : DataTypes.INTEGER,
            TGLMULAI : DataTypes.DATE,
            Jenis : DataTypes.STRING,
            project_title : DataTypes.STRING
        },
        {
            freezeTableName : true,
            timestamps : false
        }
    )
}