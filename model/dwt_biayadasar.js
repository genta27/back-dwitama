"use strict";

module.exports = (db,DataTypes) =>{
    return db.define(
        "dwt_biayadasar",
        {
            KD_biaya : {
                type : DataTypes.STRING,
                primaryKey : true,
            },
            Nama : DataTypes.STRING,
            nilai : DataTypes.INTEGER,
            Kode : DataTypes.STRING
        },{
            freezeTableName : true,
            timestamps:false
        }
    )
}