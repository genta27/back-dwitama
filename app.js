var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const con = require('./config/db');

var cookieSession = require('cookie-session')
const session = require("express-session");
const flush = require("connect-flash");

var RedisStore = require('connect-redis')(session);

var loginRouter = require('./routes/loginRouter');
var dashboardRouter = require('./routes/dashboard-router');
var app = express();

app.locals.baseURL = "http://localhost:5000/"
app.set('trust proxy', 1) // trust first proxy
// view engine setup
// app.use(EJSLayout);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2'],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))

app.use(flush());


app.use('/',loginRouter);
app.use('/dashboard',dashboardRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
